#include <iostream>
#include "OnlineVid.h"

using namespace std;

//default constructor
OnlineVid::OnlineVid(){
	//initialize all data members
	views = 0;
	id = 0;
	
	
}
//overloaded contructor
OnlineVid::OnlineVid(unsigned val, int var){
	//initialize all data members
	views = val;
	id = var;
}

 void OnlineVid::setViews(unsigned val )
 {
   views = val;
 }

 unsigned OnlineVid::getViews()
 {
  return views;
 }
 
 void OnlineVid::setId(int var)
 {
	 id = var;
 }
 
 int OnlineVid::getId(){
	 return id;
 }
