#ifndef OnlineVid_H
#define OnlineVid_H

class OnlineVid
 {
    private:  //properties
         unsigned views;
         int id; 
         


    public:  //methods
        
         OnlineVid();//default constructor 
         OnlineVid(int, unsigned);//overloaded constructor

          void setViews(unsigned);
          unsigned getViews();
          
          void setId(int);
          int getId();
          
 }//end of class
#endif
